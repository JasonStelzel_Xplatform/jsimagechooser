/*
	Manages the second-level collection view, a grid of photos in a collection (or all photos).
 */


import UIKit
import Photos
import PhotosUI

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}

extension PHAsset {
    
    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
}

public class JSAssetGridViewController: UICollectionViewController {
    
    public var fetchResult: PHFetchResult<PHAsset>!
    var assetCollection: PHAssetCollection!
    var assetPaths = [String]()
    var assetsDict: [String: PHAsset] = [:]
    var assetsUrlPath: [String: String] = [:]

    fileprivate let imageManager = PHCachingImageManager()
    fileprivate var thumbnailSize: CGSize!
    fileprivate var previousPreheatRect = CGRect.zero

    // MARK: UIViewController / Lifecycle

    override public func viewDidLoad() {
        super.viewDidLoad()

        resetCachedAssets()
        PHPhotoLibrary.shared().register(self)
        // We should arrive here by clicking one of the TableView Folders ("All Photos")
        // If we get here without a segue, it's because we're visible at app launch,
        // so match the behavior of segue from the default "All Photos" view.
        if fetchResult == nil {
            let allPhotosOptions = PHFetchOptions()
            allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            fetchResult = PHAsset.fetchAssets(with: allPhotosOptions)
        } 
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(JSAssetGridViewController.doneSelecting))
        self.navigationItem.leftBarButtonItem = doneButton

   }

    @objc func doneSelecting() {
        print ("Done selecting...", self.presentingViewController?.description as Any)
        print ("Paths = ", assetsUrlPath)
        
        
        // send new array of paths in Notification
        NotificationCenter.default.post(name: Notification.Name("doneSelectingPhotos"), object: nil, userInfo: self.assetsUrlPath)
        DispatchQueue.main.async {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Determine the size of the thumbnails to request from the PHCachingImageManager
        let scale = UIScreen.main.scale
        let cellSize = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize
        thumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
    }

    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateCachedAssets()
    }
    
    // MARK: UICollectionView
    
    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let asset: PHAsset = fetchResult.object(at: indexPath.item)
        self.toggleAssetInSelection(asset: asset) // should be sent to a serial queue
    }

    override public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print ("Fetched ", fetchResult as Any)
        return fetchResult.count
    }

    override public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let asset = fetchResult.object(at: indexPath.item)

        // Dequeue a GridViewCell.
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: JSGridViewCell.self), for: indexPath) as? JSGridViewCell
            else { fatalError("unexpected cell in collection view") }

        // Add a badge to the cell if the PHAsset represents a Live Photo.
        if asset.mediaSubtypes.contains(.photoLive) {
            cell.livePhotoBadgeImage = PHLivePhotoView.livePhotoBadgeImage(options: .overContent)
        }
        
        if ((self.assetsDict[asset.localIdentifier] ) != nil) {
            cell.selectedBadgeImage = PHLivePhotoView.livePhotoBadgeImage(options: .overContent)
            print (asset.localIdentifier + "-> SELECTED!!!")
        }
        
        // Request an image for the asset from the PHCachingImageManager.
        cell.representedAssetIdentifier = asset.localIdentifier
        cell.showHighlight = false
        imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: { image, _ in
            // The cell may have been recycled by the time this handler gets called;
            // set the cell's thumbnail image only if it's still showing the same asset.
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.thumbnailImage = image
          }
        })
        return cell
    }
    
    func procAssets() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            for asset in self.assetsDict.values {
                print ("PROC ASSET ", asset.localIdentifier)
                self.addPath(asset: asset)
            }
        }
    }
    
    // for processing final list
    func addPath(asset: PHAsset) {
        DispatchQueue.main.async {
            asset.getURL(){
                url in print("URL = ", url!.path)
                self.assetPaths.append(url!.path)
            }
        }
    }


    // MARK: UIScrollView

    override public func scrollViewDidScroll(_ scrollView: UIScrollView) {
         updateCachedAssets()
    }

    // MARK: Asset Caching

    fileprivate func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        previousPreheatRect = .zero
    }

    fileprivate func updateCachedAssets() {
        // Update only if the view is visible.
        guard isViewLoaded && view.window != nil else { return }

        // The preheat window is twice the height of the visible rect.
        let visibleRect = CGRect(origin: collectionView!.contentOffset, size: collectionView!.bounds.size)
        let preheatRect = visibleRect.insetBy(dx: 0, dy: -0.5 * visibleRect.height)

        // Update only if the visible area is significantly different from the last preheated area.
        let delta = abs(preheatRect.midY - previousPreheatRect.midY)
        guard delta > view.bounds.height / 3 else { return }

        // Compute the assets to start caching and to stop caching.
        let (addedRects, removedRects) = differencesBetweenRects(previousPreheatRect, preheatRect)
        let addedAssets = addedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }
        let removedAssets = removedRects
            .flatMap { rect in collectionView!.indexPathsForElements(in: rect) }
            .map { indexPath in fetchResult.object(at: indexPath.item) }

        // Update the assets the PHCachingImageManager is caching.
        imageManager.startCachingImages(for: addedAssets,
            targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        imageManager.stopCachingImages(for: removedAssets,
            targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)

        // Store the preheat rect to compare against in the future.
        previousPreheatRect = preheatRect
    }

    fileprivate func differencesBetweenRects(_ old: CGRect, _ new: CGRect) -> (added: [CGRect], removed: [CGRect]) {
        if old.intersects(new) {
            var added = [CGRect]()
            if new.maxY > old.maxY {
                added += [CGRect(x: new.origin.x, y: old.maxY,
                                    width: new.width, height: new.maxY - old.maxY)]
            }
            if old.minY > new.minY {
                added += [CGRect(x: new.origin.x, y: new.minY,
                                    width: new.width, height: old.minY - new.minY)]
            }
            var removed = [CGRect]()
            if new.maxY < old.maxY {
                removed += [CGRect(x: new.origin.x, y: new.maxY,
                                      width: new.width, height: old.maxY - new.maxY)]
            }
            if old.minY < new.minY {
                removed += [CGRect(x: new.origin.x, y: old.minY,
                                      width: new.width, height: new.minY - old.minY)]
            }
            return (added, removed)
        } else {
            return ([new], [old])
        }
    }

    // MARK: UI Actions
    
    func toggleAssetInSelection(asset: PHAsset) {
        DispatchQueue.main.async {
            if (self.assetsDict[asset.localIdentifier] != nil) {
                self.assetsDict[asset.localIdentifier] = nil
                self.removeUrlPathFromDict(asset: asset)
            } else {
                self.assetsDict[asset.localIdentifier] = asset
                self.addUrlPathToDict(asset: asset)
            }
            self.collectionView.reloadData()
            print ("TOTAL SO FAR = ", self.assetsDict)
            self.showPath(asset: asset)
       }
    }
    
    func addUrlPathToDict(asset: PHAsset) {
        DispatchQueue.main.async {
            asset.getURL(){
                url in print("URL = ", url!.path)
                self.assetsUrlPath[asset.localIdentifier] = url!.path
            }
        }
    }
    
    func removeUrlPathFromDict(asset: PHAsset) {
        DispatchQueue.main.async {
            self.assetsUrlPath[asset.localIdentifier] = nil
        }
    }
    
    
    func showPath(asset: PHAsset) {
        PHImageManager.default().requestImageData(for: asset, options: nil, resultHandler: { _, _, _, info in
            if let fileName = (info?["PHImageFileURLKey"] as? NSURL) {
                print ("PathURL as text = ", fileName.path!)
            }
        })
    }

    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? JSAssetViewController
            else { fatalError("unexpected view controller for segue") }
        
        let indexPath = collectionView!.indexPath(for: sender as! UICollectionViewCell)!
        destination.asset = fetchResult.object(at: indexPath.item)
        destination.assetCollection = assetCollection
    }
}

// MARK: PHPhotoLibraryChangeObserver
extension JSAssetGridViewController: PHPhotoLibraryChangeObserver {
    public func photoLibraryDidChange(_ changeInstance: PHChange) {

        guard let changes = changeInstance.changeDetails(for: fetchResult)
            else { return }

        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            fetchResult = changes.fetchResultAfterChanges
            if changes.hasIncrementalChanges {
                // If we have incremental diffs, animate them in the collection view.
                guard let collectionView = self.collectionView else { fatalError() }
                collectionView.performBatchUpdates({
                    // For indexes to make sense, updates must be in this order:
                    // delete, insert, reload, move
                    if let removed = changes.removedIndexes, removed.count > 0 {
                        collectionView.deleteItems(at: removed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let inserted = changes.insertedIndexes, inserted.count > 0 {
                        collectionView.insertItems(at: inserted.map({ IndexPath(item: $0, section: 0) }))
                    }
                    if let changed = changes.changedIndexes, changed.count > 0 {
                        collectionView.reloadItems(at: changed.map({ IndexPath(item: $0, section: 0) }))
                    }
                    changes.enumerateMoves { fromIndex, toIndex in
                        collectionView.moveItem(at: IndexPath(item: fromIndex, section: 0),
                                                to: IndexPath(item: toIndex, section: 0))
                    }
                })
            } else {
                // Reload the collection view if incremental diffs are not available.
                collectionView!.reloadData()
            }
            resetCachedAssets()
        }
    }
}

