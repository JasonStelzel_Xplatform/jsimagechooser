/*
	Manages the top-level table view, a list of photo collections.
 */


import UIKit
import Photos

public class JSImageFolderViewController: UIViewController, PHPhotoLibraryChangeObserver {
    public func photoLibraryDidChange(_ changeInstance: PHChange) {
        print ("Don't worry about it")
    }
    
    @IBOutlet var ShowPhotos: UIButton!
    @IBOutlet var ShowVideos: UIButton!
    @IBOutlet var ShowAll: UIButton!
    
    // MARK: Types for managing sections, cell and segue identifiers
    public enum Section: Int {
        case showAll = 0
        case show
        case smartAlbums
        case userCollections

        static let count = 3
    }

    public enum CellIdentifier: String {
        case showAll, collection
    }

    public enum SegueIdentifier: String {
        case showAllPhotos
        case showCollection
    }
    

    // MARK: Properties
    var fetchAll: PHFetchResult<PHAsset>!
    // MARK: UIViewController / Lifecycle
    
    override public func viewDidLoad() {
        print ("Image Folders view did load")
        super.viewDidLoad()
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addAlbum))
        self.navigationItem.rightBarButtonItem = addButton
        
//        self.title = "MY PHOTOS"

        // Create a PHFetchResult object for each section in the table view.
//        fetchAll = PHAsset.fetchAssets(with: allPhotosOptions)
        PHPhotoLibrary.shared().register(self as PHPhotoLibraryChangeObserver)
    }

    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self as PHPhotoLibraryChangeObserver)
    }

    override public func viewWillAppear(_ animated: Bool) {
    //    self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    @objc func addAlbum(_ sender: AnyObject) {

        let alertController = UIAlertController(title: NSLocalizedString("New Album", comment: ""), message: nil, preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = NSLocalizedString("Album Name", comment: "")
        }

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Create", comment: ""), style: .default) { action in
            let textField = alertController.textFields!.first!
            if let title = textField.text, !title.isEmpty {
                // Create a new album with the title entered.
                PHPhotoLibrary.shared().performChanges({
                    PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: title)
                }, completionHandler: { success, error in
                    if !success { print("error creating album: \(String(describing: error))") }
                })
            }
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ShowAll":
            print ("Show All")
        case "ShowPhotos":
            print ("Show Photos")
            guard let destination = segue.destination as? JSAssetGridViewController
                else { fatalError("unexpected view controller for segue") }
            let allPhotosOptions = PHFetchOptions()
//            allPhotosOptions.predicate = NSPredicate(format: "(mediaType & %d) != 0", "image")
            allPhotosOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
            allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            let fetchPhotos = PHAsset.fetchAssets(with: allPhotosOptions)
           destination.fetchResult = fetchPhotos
        case "ShowVideos":
            print ("Show Videos")
            guard let destination = segue.destination as? JSAssetGridViewController
                else { fatalError("unexpected view controller for segue") }
            let allVideosOptions = PHFetchOptions()
            allVideosOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.video.rawValue)
            allVideosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            let fetchVideos = PHAsset.fetchAssets(with: allVideosOptions)
            destination.fetchResult = fetchVideos
        default:
            print ("Show All")
        }
    }

//    // MARK: Table View
//
//    override public func numberOfSections(in tableView: UITableView) -> Int {
////        print ("Number of sections ", Section.count)
//        return Section.count
//    }
//
//    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        print ("Section ", Section(rawValue: section)!)
//        switch Section(rawValue: section)! {
//            case .showAll: return 1
//            case .smartAlbums: return smartAlbums.count
//            case .userCollections: return userCollections.count
//        }
//    }
//
//    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////        print ("Cell ID ", Section(rawValue: indexPath.section)!, CellIdentifier.showAll.rawValue)
//        print (".showAll = ", CellIdentifier.showAll)
//       switch Section(rawValue: indexPath.section)! {
//            case .showAll:
//                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.showAll.rawValue, for: indexPath)
//                cell.textLabel!.text = NSLocalizedString("All Photos", comment: "")
//                return cell
//
//            case .smartAlbums:
//                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.collection.rawValue, for: indexPath)
//                let collection = smartAlbums.object(at: indexPath.row)
//                cell.textLabel!.text = collection.localizedTitle
//                return cell
//
//            case .userCollections:
//                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.collection.rawValue, for: indexPath)
//                let collection = userCollections.object(at: indexPath.row)
//                cell.textLabel!.text = collection.localizedTitle
//                return cell
//        }
//    }
//
//    override public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return sectionLocalizedTitles[section]
//    }
//
//}
//
//// MARK: PHPhotoLibraryChangeObserver
//extension JSImageFolderViewController: PHPhotoLibraryChangeObserver {
//
//    public func photoLibraryDidChange(_ changeInstance: PHChange) {
//        // Change notifications may be made on a background queue. Re-dispatch to the
//        // main queue before acting on the change as we'll be updating the UI.
//        DispatchQueue.main.sync {
//            // Check each of the three top-level fetches for changes.
//
//            if let changeDetails = changeInstance.changeDetails(for: showAll) {
//                // Update the cached fetch result.
//                showAll = changeDetails.fetchResultAfterChanges
//                // (The table row for this one doesn't need updating, it always says "All Photos".)
//            }
//
//            // Update the cached fetch results, and reload the table sections to match.
//            if let changeDetails = changeInstance.changeDetails(for: smartAlbums) {
//                smartAlbums = changeDetails.fetchResultAfterChanges
//                tableView.reloadSections(IndexSet(integer: Section.smartAlbums.rawValue), with: .automatic)
//            }
//            if let changeDetails = changeInstance.changeDetails(for: userCollections) {
//                userCollections = changeDetails.fetchResultAfterChanges
//                tableView.reloadSections(IndexSet(integer: Section.userCollections.rawValue), with: .automatic)
//            }
//
//        }
//    }
    
    
}

