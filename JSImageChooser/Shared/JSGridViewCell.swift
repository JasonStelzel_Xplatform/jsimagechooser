/*
	Collection view cell for displaying an asset.
 */


import UIKit

public class JSGridViewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var livePhotoBadgeImageView: UIImageView!
    @IBOutlet var selectedBadgeImageView: UIImageView!
    
    var pathToAsset: String!
    var representedAssetIdentifier: String!
    var showHighlight: Bool!
//    let plus = UIImage(named: "icons8-ok-30")
    let plus = UIImage(named: "BlueCheck")

    var thumbnailImage: UIImage! {
        didSet {
            imageView.image = thumbnailImage
            if (showHighlight) {imageView.layer.borderWidth = 8}

        }
    }
    var livePhotoBadgeImage: UIImage! {
        didSet {
            livePhotoBadgeImageView.image = livePhotoBadgeImage
        }
    }
    
    var selectedBadgeImage: UIImage! {
       didSet {
            selectedBadgeImageView.image = plus
        }
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        livePhotoBadgeImageView.image = nil
        selectedBadgeImageView.image = nil
        representedAssetIdentifier = nil
    }
}
